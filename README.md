# Social Media Actions #

### What is this repository for? ###
This package handles common social media actions like: 

Comments, Bookmarks, Likes, Views

this package requires a users table for managing the owner of the actions


#### Requirements:
- `php >= 7.3`

### Installation ###

Run these commands

    1- $ composer require hadishahpuri/social-media-actions
    
    2- $ php artisan actions:install

3- in configs folder open 
### Usage ###

* Writing tests
* Code review
* Other guidelines

### Who do I talk to? ###

* Repo owner or admin
* Other community or team contact